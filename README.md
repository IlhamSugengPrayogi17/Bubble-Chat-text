# Bubble Chat Text

Create Bubble Chat Text in Unity from JSON file

## Author

4210181022 Ilham Sugeng Prayogi

## Mata Kuliah
Praktikum Desain Multiplayer Game Online

## Implemented Text from JSON File to Unity Text

#### Here is the full code
[MainCode](Assets/Script/JSONConvertScript.cs)

#### Here is the GIF
![DemoGIF](/4210181022-Ilham_Sugeng_Prayogi.gif)

#### Here is the Flow diagram
![Flow](/Flow.png)

#### We will use SimpleJSON as API to Parser the JSON
#### Add this code to Namespace, to use SimpleJSON
#### `using SimpleJSON`;

#### Create Class to Get JSON from URL 
```
private void Awake()
    {
        WWW request = new WWW(LinkJSON);
        StartCoroutine(proccessReq(request));
    }
```
We can retrieving JSON file from certain URL, that we can input in Unity.

#### Get the text from JSON and field it.
```
private string getName(string json)
    {
        JSONArray Array = JSON.Parse(json).AsArray;
        return Array[index]["name"].Value;
    }

    private string getEmail(string json)
    {
        JSONArray Array = JSON.Parse(json).AsArray;
        return Array[index]["email"].Value;
    }
```
This code will execute to Deserialize JSON to String
