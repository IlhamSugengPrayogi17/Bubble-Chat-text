﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using TMPro;

public class JSONConvertScript : MonoBehaviour
{
    public int index;
    public float timedelay = 2.0f;
    public string LinkJSON;
    public TMP_Text textBox;
    public GameObject BubbleObject;
    private bool Bubble = false;

    private void Awake()
    {
        WWW request = new WWW(LinkJSON);
        StartCoroutine(proccessReq(request));
    }
    void Start()
    {
        if (Bubble == false)
        {
            BubbleObject.gameObject.SetActive(false);
        }
    }

    IEnumerator proccessReq(WWW Req)
    {
        yield return Req;
        Bubble = true;
        if (Bubble == true)
        {
            BubbleObject.gameObject.SetActive(true);
        }
        yield return new WaitForSeconds(timedelay);
        textBox.text = "My name is " + getName(Req.text);

        yield return new WaitForSeconds(timedelay);
        textBox.text = "You can contact me at " + getEmail(Req.text);
    }

    private string getName(string json)
    {
        JSONArray Array = JSON.Parse(json).AsArray;
        return Array[index]["name"].Value;
    }

    private string getEmail(string json)
    {
        JSONArray Array = JSON.Parse(json).AsArray;
        return Array[index]["email"].Value;
    }
}
